/** 
  PD: en el PDF enviado no contenia indicaciónes de animación si requieren una 
  animación mas compleja me lo hacen saber, no cuento con tiempo asi que lo he
  realizado el dia Miercoles a la 1am.
**/
(function() {
    var _ = jQuery,
            App;
    App = function() {
        this.index = 0;
        this.value = 0;
        this.msg = '';
    }
    App.prototype = {
        arr: [],
        start: function(number) {
            var main = this;
            number.on('keydown', function() {
                return (_(this).val().length > 2) ? false : 1;
            });
            _('#add').click(function() {
                main.number = number.val();
                obj.verify();
            });
            _('#order').click(function() {
                obj.sortNumbers();
            });
        },
        // order numbers and apply animation
        sortNumbers: function() {
            var positions = [],
                temp = [];
            this.arr.sortNumbers();
            this.arr.forEach(function(value, item, arr) {
                temp['left'] = _('.index-' + value)[0].offsetLeft;
                setTimeout(function() {
                    _('.index-' + value).animate({
                        'left': ((item === 0) ? '0' : ((item) * 100))
                    });
                }, item * 100);
                temp['index'] = value;
                positions.push(temp);
                temp = [];
            });
        },
        // verify if number exist
        verify: function() {
            if (this.number != '') {
                if (this.arr.indexOf(this.number) < 0) {
                    var len = _('.item').length;
                    _('.msg').removeClass('error').text('');
                    this.arr.push(this.number);
                    _('input[type="number"]').val('');
                    _('.integers').append('<div class="item index-' + this.number + '" style="left:' + ((len === 0) ? '0' : ((len) * 100)) + 'px">\
                                        <p>' + this.number + '</p>\
                                      </div>');
                } else this.showMsg(1);
            } else this.showMsg(2);
        },
        // show status messages from service
        showMsg: function(status) {
            var msg = _('.msg');
            switch (status) {
                case 1:
                    this.msg = 'El numero ya fue ingresado.';
                    break;
                case 2:
                    this.msg = 'Algo esta mal.';
                    break;
            }
            msg.addClass('error').text(this.msg);
            setTimeout(function() {
                this.msg = '';
                msg.removeClass('error').text('');
            }, 1000);
        }
    }
    Array.prototype.sortNumbers = function() {
        return this.sort(function(a, b) {
            return a - b
        });
    }
    // Start App
    var obj = new App();
    obj.start(_('input[type="number"]'));
}(jQuery));